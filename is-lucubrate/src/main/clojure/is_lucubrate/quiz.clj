(ns is-lucubrate.quiz
  (:gen-class)
  (:use is-lucubrate.is-util)
  (:use clojure.set)
  (:require [clojure.java.io :as io]
            [clojure.data.json :as json]
            [clojure.string :as str]))

(defn load-question-set
  [x]
  "Loads a question set"
  (marshall-json (clojure.string/join ["json/osa/" x ".json"]))
  )

(defn split-by-spaces
  [x]
  (str/split x #" "))

(defn ask
  "Asks the question and returns the answers as an array"
  [question]
  (do
    (println question)
    (split-by-spaces (readline))))

(defn score
  "Returns 1 if actual matches expected"
  [expected actual]
  (if (= expected actual)
    1
    0))

(defn score-all
  "Counts the number of correct results"
  [expected actual]
  (loop [i 0 s 0]
    (if (nil? (get expected i))
      s
      (recur (inc i) (+ s (score (get expected i) (get actual i)))))))

(defn count-max-score
  "Counts the maximum score possible for this quiz"
  [qset]
  (count (mapcat :answers qset)))

(defn process
  "Processes A Question"
  [qa]
  (do
    (let [ans (:answers qa)]
      (let [sc (score-all (ask (:question qa)) ans)]
        (println "Answer: " ans " Score: " sc)
        (int sc)))))

(defn start-quiz
  "Starts the Quiz"
  [question-set]
  (let [qs (:questions question-set)]
    (println "Starting Quiz: " (:title question-set))
    (let [co (reduce + (map process (shuffle qs)))]
      (println "You scored " co " out of " (count-max-score qs)))))