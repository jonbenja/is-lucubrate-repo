(ns is-lucubrate.is-util
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io])
  (:import (java.io BufferedReader)))

(defn not-nil? [x] (not (nil? x)))

(defn ! [x] (not x))

(defn readline
  "Reads a line from the console and returns it"
  []
  (let [reader (BufferedReader. *in*)
        ln (.readLine reader)]
  (str ln)))

(defn classpath-to-string
  "Reads a File from the classpath to a String"
  [path]
  (slurp
    (io/file
      (io/resource
        path))))

(defn marshall-json
  "parse json-string into map"
  [qset]
  (json/read-str
    (classpath-to-string qset) :key-fn keyword))

;(println "Hello" (readline))