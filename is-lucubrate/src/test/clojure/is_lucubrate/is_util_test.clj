(ns is-lucubrate.is-util-test
  (:use clojure.test)
  (:use is-lucubrate.is-util)
  (:require [clojure.data.json :as json]
            [clojure.java.io :as io])
  (:import (java.util Map)))

(deftest !-test
  (is (= false (! true))))

(deftest not-nil-test
  (is (= false (not-nil? nil))))


(deftest classpath-to-string-test
  (testing "Classpath To String"
    (let [x (classpath-to-string "json/osa/slow_talk.json")]
      (do
        ;(println x)
        (is (not-nil? x))
        (is (instance? String x))))))

(deftest marshall-json-test
  (testing "Marshall Json File to Map"
    (let [x (marshall-json "json/osa/slow_talk.json")]
      (do
        (println "Title: " (:title x))
        (is (instance? Map x))
        (is (= (:title x) "Slow Talk")))
      )))

(run-all-tests)