(ns is-lucubrate.quiz-test
  (:use clojure.test)
  (:use is-lucubrate.quiz)
  )

(def question-set (:questions (load-question-set "slow_talk")))

;(println question-set)
;(println (count (mapcat :answers question-set)))

(deftest score-all-test
  (is (= 2 (score-all ["Hello World", "ABC" "" "" "" "" ""]
                      ["Hello World" "ABC" 1 -1 "kjhkhjghkj"]))))

(deftest count-max-score-test
  (is (= 11 (count-max-score question-set))))

(run-all-tests)